#include "Utils.h"

//延时nus
//注意nus的范围
//SysTick->LOAD为24位寄存器,所以,最大延时为:
//nus<=0xffffff*8*1000000/SYSCLK
//SYSCLK单位为Hz,nus单位为us
//对72M条件下,nus<=1864135
void SysTickDelayUs(uint32_t nus)
{
  uint32_t temp;
  SysTick->LOAD = nus * (SystemCoreClock/8000000);
  SysTick->VAL = 0x00;  //清空计数器
  SysTick->CTRL = SysTick_CTRL_ENABLE_Msk;  //选择外部时钟源(HCLK/8)并开启SysTick
  do {
    temp = SysTick->CTRL;
  }while((temp&0x01) && !(temp&SysTick_CTRL_COUNTFLAG_Msk));  //等待时间到达
  SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;  //关闭SysTick
  SysTick->VAL = 0X00;  //清空计数器
}

//延时nms
//注意nms的范围
//SysTick->LOAD为24位寄存器,所以,最大延时为:
//nms<=0xffffff*8*1000/SYSCLK
//SYSCLK单位为Hz,nms单位为ms
//对72M条件下,nms<=1864
void SysTickDelayMs(uint32_t nms)
{
  uint32_t temp;
  SysTick->LOAD = nms * (SystemCoreClock/8000);
  SysTick->VAL = 0x00;  //清空计数器
  SysTick->CTRL = SysTick_CTRL_ENABLE_Msk;  //选择外部时钟源(HCLK/8)并开启SysTick
  do {
    temp = SysTick->CTRL;
  }while((temp&0x01) && !(temp&SysTick_CTRL_COUNTFLAG_Msk));  //等待时间到达
  SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;  //关闭SysTick
  SysTick->VAL = 0X00;  //清空计数器
}

